﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Login
{
    public partial class dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            userDetails.Text = "Welcome " + Session["UserName"];

            if(Session["UserName"] == null) // if the username session contains no details
                Response.Redirect("/index.aspx");
        }

        protected void logOutBtn_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("/index.aspx");
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="register.aspx.cs" Inherits="Login.register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        body{
            text-align: center;
        }
         #userNameTxt, #passWordTxt, #confirmPassWordTxt{
             font-size: 15px;
             width: 347px;
             height: 35px;
             margin: 5px;
             border: 1px solid green;
             margin-bottom: 10px;
             padding-left: 5px;
         }
         #formControl{
             width: 40%;
             display: inline-block;
             height: 350px;
             margin-top: 60px;
             border: 1px solid rgb(11, 119, 65);
             padding-top: 50px;
         }
         #createAccount {
              background: rgb(11, 119, 65);
              width: 350px;
              height: 40px;
              border: none;
              font-size: 16px;
              color: white;
              
         }
         #createAcc{
             font-size: 30px;
             color: rgb(11, 119, 65);
            margin-bottom: 100px;
         }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        
        <div id="formControl">
            <asp:Label ID="createAcc" runat="server" Text="Create Account" Font-Bold="True"></asp:Label>
            <br />  <br />  <br />
            <asp:TextBox ID="userNameTxt" runat="server" Text="" placeholder="User Name" ></asp:TextBox> 

            <br />  
            <asp:TextBox ID="passWordTxt" runat="server" placeholder="Password" TextMode="Password"></asp:TextBox> 
            
            <br />  
            <asp:TextBox ID="confirmPassWordTxt" runat="server" placeholder="Confirm Password" TextMode="Password"></asp:TextBox> 
            
            <br /> 

            <asp:Button ID="createAccount" runat="server" Text="Create Account" OnClick="createAccount_Click" /> <br /> <br />
            <asp:Label ID="errorMsg" runat="server" ForeColor="Red"></asp:Label>
        </div>
    </form>
</body>
</html>

﻿using Login.Services;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Login
{
    public partial class register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //createAccount.Enabled = false;
        }

        protected void createAccount_Click(object sender, EventArgs e)
        {
            try
            {
                if (passWordTxt.Text != confirmPassWordTxt.Text)
                {
                    errorMsg.Text = "Password Mismatch!";
                }
                else if (string.IsNullOrEmpty(userNameTxt.Text) || string.IsNullOrEmpty(passWordTxt.Text) || string.IsNullOrEmpty(confirmPassWordTxt.Text))
                {
                    errorMsg.Text = "Fields cannot be empty!";
                }
                else
                {
                    //createAccount.Enabled = true;
                    DataBaseConnection databaseConnection = new DataBaseConnection();
                    databaseConnection.OpenConnection();

                    // string query = "SELECT COUNT(1) FROM users WHERE UserName=@UserName AND PassWord=@Password";
                    string query = "INSERT INTO users (UserName, PassWord) VALUES (@UserName, @PassWord)";
                    SqlCommand sqlCmd = new SqlCommand(query, databaseConnection.sqlCon);

                    sqlCmd.Parameters.AddWithValue("@UserName", userNameTxt.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@PassWord", passWordTxt.Text);
                   //  sqlCmd.Parameters.AddWithValue("@id", "@id");

                    var result = sqlCmd.ExecuteNonQuery();
                    databaseConnection.CloseConnection();

                    Session["AcountCreated"] = "Account Created successfully";
                    // check index for session
                    errorMsg.Text = "Account created!";
                    
                    Response.Redirect("/Index.aspx");
                }
            }
            catch (Exception ex)
            {

                errorMsg.Text = ex.Message;
            }
        }

        protected void confirmPassWordTxt_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
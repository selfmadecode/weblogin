﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Login.Services;

namespace Login
{
    public partial class Index : System.Web.UI.Page
    {
        // string conncetionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\SelfMade\source\repos\Login\App_Data\users.mdf;Integrated Security=True";
        protected void Page_Load(object sender, EventArgs e)
        {
            errorMsg.Visible = false;
        }

        protected void loginBtb_Click(object sender, EventArgs e)
        {
            try
            {
                // string conncetionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\SelfMade\source\repos\Login\App_Data\users.mdf;Integrated Security=True";
               
                if (string.IsNullOrEmpty(userNameTxt.Text) || string.IsNullOrEmpty(passWordTxt.Text))
                {
                    // System.Windows.Forms.MessageBox.Show("Test");
                    errorMsg.Text = "Feilds cannot be empty! ";
                    return;
                }
                
                DataBaseConnection databaseConnection = new DataBaseConnection();
                databaseConnection.OpenConnection();

                string query = "SELECT COUNT(1) FROM users WHERE UserName=@UserName AND PassWord=@Password";

                SqlCommand sqlCmd = new SqlCommand(query, databaseConnection.sqlCon);

                sqlCmd.Parameters.AddWithValue("@UserName", userNameTxt.Text.Trim());
                sqlCmd.Parameters.AddWithValue("@Password", passWordTxt.Text.Trim());

                int count = Convert.ToInt32(sqlCmd.ExecuteScalar()); // gets the number of users with username and password

                if (count == 1) // if the userdetails is in the database
                {
                    Session["UserName"] = userNameTxt.Text.Trim(); // stores the user details in a session
                    Response.Redirect("Views/dashboard.aspx");
                }
                else
                {
                    errorMsg.Visible = true;
                }

                databaseConnection.CloseConnection();

            }
            catch (Exception ex)
            {

                errorMsg.Text = ex.Message;
                Console.WriteLine(ex.Message);
            }

            // errorMsg.Text = "Login! " + Session["AccountCreated"];

            /////
            //using (SqlConnection sqlCon = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\SelfMade\source\repos\Login\App_Data\users.mdf;Integrated Security=True"))
            //{
            //    sqlCon.Open();
            //    string query = "SELECT COUNT(1) FROM users WHERE UserName=@UserName AND PassWord=@Password";
            //    SqlCommand sqlCmd = new SqlCommand(query, sqlCon);

            //    sqlCmd.Parameters.AddWithValue("@UserName", userNameTxt.Text.Trim());
            //    sqlCmd.Parameters.AddWithValue("@Password", passWordTxt.Text.Trim());

            //    int count = Convert.ToInt32(sqlCmd.ExecuteScalar()); // gets the number of users with username and password
            //    if (count == 1) // if the user is in the database
            //    {
            //        Session["UserName"] = userNameTxt.Text.Trim(); // stores the user details in a session
            //        Response.Redirect("dashboard.aspx");
            //    }
            //    else
            //    {
            //        errorMsg.Visible = true;
            //    }
            //}
        }

        protected void createAccountBtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("Views/register.aspx");
        }
    }
}
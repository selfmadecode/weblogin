﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Login.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        body{
            text-align: center;
        }
        #loginBtn, #createAccountBtn {
              background: rgb(11, 119, 65);
              width: 350px;
              height: 40px;
              border: none;
              font-size: 16px;
              color: white;
              
        }
        #userNameTxt, #passWordTxt{
             font-size: 15px;
             width: 347px;
             height: 35px;
             margin: 5px;
             border: 1px solid green;
             margin-bottom: 10px;
             padding-left: 5px;
         }
        #formControl{
             width: 40%;
             display: inline-block;
             height: 350px;
             margin-top: 60px;
             border: 1px solid rgb(11, 119, 65);
             padding-top: 50px;
         }
        #loginLbl{
             font-size: 30px;
             color: rgb(11, 119, 65);
            margin-bottom: 100px;
         }

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="formControl">
            <div>
                <asp:Label ID="loginLbl" runat="server" Text="Login" Font-Bold="True"></asp:Label> <br />

                <asp:TextBox ID="userNameTxt" runat="server" placeholder="user Name"></asp:TextBox> <br />

                <asp:TextBox ID="passWordTxt" runat="server" TextMode="Password" placeholder="Password"></asp:TextBox> <br />
                
                <asp:Button ID="loginBtn" runat="server" Text="Login" OnClick="loginBtb_Click" /> <br />  <br />  
                
                <asp:Button ID="createAccountBtn" runat="server" Text="Create Account" OnClick="createAccountBtn_Click" /> <br />   
                <asp:Label ID="errorMsg" runat="server" Text="Details not found" ForeColor="Red"></asp:Label>
                
            </div>
        </div>
    </form>
</body>
</html>

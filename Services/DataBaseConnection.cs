﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace Login.Services
{
    public class DataBaseConnection
    {
        public SqlConnection sqlCon;

        public DataBaseConnection()
        {
            sqlCon = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\SelfMade\source\repos\Login\App_Data\users.mdf;Integrated Security=True");
        }
        public void OpenConnection()
        {
            if(sqlCon.State != ConnectionState.Open)
            {
                sqlCon.Open();
            }
        }
        public void CloseConnection()
        {
            if (sqlCon.State != ConnectionState.Closed)
            {
                sqlCon.Close();
            }
        }
    }
}